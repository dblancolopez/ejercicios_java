import java.util.Scanner;
class InfoNumsK {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int max = 0;
        int min = 0;
        double media;
        int num = -1;
        boolean primer = true;
        int numVeces = 0 ;

        do {
            System.out.printf("Entra un num: ");
            try {
                num = keyboard.nextInt();
                total += num;
                if(primer){
                    max = num;
                    min = num;
                    primer = false;
                }else{
                    if(max < num){
                        max = num;
                    }
                    if(num != 0 && min > num){
                        min = num;
                    }
                }  
                numVeces++; 
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num > 0);
        numVeces--;
        media = total/numVeces;

        System.out.println("Numeros introduits " + numVeces);
        System.out.println("El total es "+total);
        System.out.println("EL numero mes gran es " + max);
        System.out.println("EL numero mes petit es " + min);
        System.out.println("La mitjana aritmetica es " + media);
    }
}
