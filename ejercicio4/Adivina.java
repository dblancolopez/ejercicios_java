import java.util.Scanner;
import java.util.Random;

class Adivina {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        int num = -1;
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        boolean exito = false;
        int intentos = 0;

        do {
            System.out.printf("Entra el numero que creus que ha sortit (entre 1 i 10): ");
            try {
                num = keyboard.nextInt();
                if(num == incognita){
                    exito = true;
                }
                intentos++;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (!exito);

        System.out.println("Has adivinat el numero en l'intent " + intentos);
        keyboard.close();
    }

}