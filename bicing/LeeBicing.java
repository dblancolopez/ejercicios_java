import java.io.BufferedReader;
import java.io.*;
import java.net.*;

import jdk.nashorn.internal.ir.CatchNode;

class leeBicing {
    public static void main(String[] args) {
        conectar();
    }

    public static void conectar(){
        File dest = new File("texto.json");
        URLConnection conn = null;

        try {
            URL url = new URL("https://api.citybik.es/v2/networks/bicing");
            conn = url.openConnection();

            try (InputStream is = conn.getInputStream();
                    BufferedInputStream bis = new BufferedInputStream(is);
                    FileOutputStream fos = new FileOutputStream(dest);
                    BufferedOutputStream bos = new BufferedOutputStream(fos)) {

                int size = 1024;
                byte[] buffer = new byte[size];
                int len;
                while ((len = bis.read(buffer, 0, size)) > -1) {
                    bos.write(buffer, 0, len);
                }
                bos.flush();
                bis.close();
                bos.close();
            } catch (IOException e) {
                System.out.println("Pagina no responde.");
            }
        } catch (MalformedURLException e) {
            System.out.println("URL no valida.");
            return;
        } catch (IOException e) {
            System.out.println("Pagina no responde.");
            return;
        }

    }
}