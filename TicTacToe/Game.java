import java.util.Scanner;
import java.util.Random;
class Game {
    Scanner lector = new Scanner(System.in);

    int[] tabla = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public void setMap(int posicion, int jugador) {
        tabla[posicion] = jugador;
    }

    public int getMap(int posicion) {
        int estado = 0;

        estado = tabla[posicion];

        return estado;
    }

    /**
     * Metodo para imprimir tablero.
     */
    public void draw() {
        for (int i = 0; i < tabla.length; i++) {
            if (tabla[i] == 0) {
                System.out.print("-");
            } else if(tabla[i] == 1){
                System.out.print("X");
            }else if(tabla[i] == 2){
                System.out.print("O");
            }
            if (i == 2 || i == 5) {
                System.out.println();
            }
        }
        System.out.println();
    }

    /**
     * Metodo para comporbar la cantidad de posiciones que quedan sin ficha.
     * @return
     */
    public int numZeros() {
        int zeros = 0;

        for (int i = 0; i < tabla.length; i++) {
            if (tabla[i] == 0) {
                zeros++;
            }
        }
        return zeros;
    }

    /**
     * Metodo para comprobar ganador o empate.
     */
    public int winner(){
        int ganador = 0;
        if (// HORIZONTAL
            tabla[0] == 1 && tabla[1] == 1 && tabla[2] == 1 ||
            tabla[3] == 1 && tabla[4] == 1 && tabla[5] == 1 ||
            tabla[6] == 1 && tabla[7] == 1 && tabla[8] == 1 ||
            //VERTICAL
            tabla[0] == 1 && tabla[3] == 1 && tabla[6] == 1 ||
            tabla[1] == 1 && tabla[4] == 1 && tabla[7] == 1 ||
            tabla[2] == 1 && tabla[5] == 1 && tabla[8] == 1 ||
            //DIAGONALES
            tabla[0] == 1 && tabla[4] == 1 && tabla[8] == 1 ||
            tabla[2] == 1 && tabla[4] == 1 && tabla[6] == 1) {
            
            ganador = 1;
        } else if(// HORIZONTAL
            tabla[0] == 2 && tabla[1] == 2 && tabla[2] == 2 ||
            tabla[3] == 2 && tabla[4] == 2 && tabla[5] == 2 ||
            tabla[6] == 2 && tabla[7] == 2 && tabla[8] == 2 ||
            //VERTICAL
            tabla[0] == 2 && tabla[3] == 2 && tabla[6] == 2 ||
            tabla[1] == 2 && tabla[4] == 2 && tabla[7] == 2 ||
            tabla[2] == 2 && tabla[5] == 2 && tabla[8] == 2 ||
            //DIAGONALES
            tabla[0] == 2 && tabla[4] == 2 && tabla[8] == 2 ||
            tabla[2] == 2 && tabla[4] == 2 && tabla[6] == 2) {
           
            ganador = 2;
        } else if(numZeros() == 0){
            ganador = 3;
        }
        return ganador;
    }

    /**
     * Metodo para mostrar las posiciones de la tabla.
     */
    public void drawTabla(){
        for (int i = 0; i < tabla.length; i++) {
            System.out.print(i + 1);
            if (i == 2 || i == 5) {
                System.out.println();
            }
        }
        System.out.println();
    }


    /**
     * Metodo para mostrar mensaje de ganador o empate.
     * @param i
     */
    public void mostrarGanador(int i){
        switch(i){
            case 1:
            System.out.println("Ganador jugador 1");
            break;
            case 2:
            System.out.println("Ganador jugador 2");
            break;
            case 3:
            System.out.println("Empate");
            break;
        }
    }

    /**
     * Meotodo para cambiar el turno de jugador para version de 2 jugadores.
     * @param turnoActual
     * @return
     */
    public int cambiarTurno(int turnoActual){
        int turno;
        if (turnoActual == 1) {
            turno = 2;
        } else {
            turno = 1;
        }
        return turno;
    }


    public int logicaInteligenciaArtificial(){
        Random random = new Random();
        int jugada = 0;
        boolean posCorrecta = false;

        if(comprobarJugadasJugadores(2) != 0){
            jugada = comprobarJugadasJugadores(2);
        }else if(comprobarJugadasJugadores(1) != 0){
            jugada = comprobarJugadasJugadores(1);
        }else{
            do{
                jugada = random.nextInt(9)+1;
                if (getMap(jugada-1) == 0) {
                    posCorrecta = true;
                }
            }while(!posCorrecta);
        }
        
        return jugada -1;


        // LOGICA IA SIMPLE (ALEATORIO)

        /*Random random = new Random();
        int jugada = 0;
        boolean posCorrecta = false;
        do{
            jugada = random.nextInt(9)+1;
            if (getMap(jugada-1) == 0) {
                posCorrecta = true;
            }
        }while(!posCorrecta);
        
    return jugada -1;*/
    }

    public int comprobarJugadasJugadores(int jugador){
        int jugada;

        if (tabla[0] == jugador && tabla[1] == jugador && tabla[2] == 0) {
            jugada = 3;
        }else if(tabla[0] == jugador && tabla[1] == 0 && tabla[2] == jugador){
            jugada = 2;
        }else if(tabla[0] == 0 && tabla[1] == jugador && tabla[2] == jugador) {
            jugada = 1;
        }else if(tabla[3] == jugador && tabla[4] == jugador && tabla[5] == 0){
            jugada = 6;
        }else if(tabla[3] == jugador && tabla[4] == 0 && tabla[5] == jugador){
            jugada = 5;
        }else if(tabla[3] == 0 && tabla[4] == jugador && tabla[5] == jugador){
            jugada = 4;
        }else if(tabla[6] == jugador && tabla[7] == jugador && tabla[8] == 0){
            jugada = 9;
        }else if(tabla[6] == jugador && tabla[7] == 0 && tabla[8] == jugador){
            jugada = 8;
        }else if(tabla[6] == 0 && tabla[7] == jugador && tabla[8] == jugador){
            jugada = 7;
        }else if(tabla[0] == jugador && tabla[3] == jugador && tabla[6] == 0){
            jugada = 7;
        }else if(tabla[0] == jugador && tabla[3] == 0 && tabla[6] == jugador){
            jugada = 4;
        }else if(tabla[0] == 0 && tabla[3] == jugador && tabla[6] == jugador){
            jugada = 1;
        }else if(tabla[1] == jugador && tabla[4] == jugador && tabla[7] == 0){
            jugada = 8;
        }else if(tabla[1] == jugador && tabla[4] == 0 && tabla[7] == jugador){
            jugada = 5;
        }else if(tabla[1] == 0 && tabla[4] == jugador && tabla[7] == jugador){
            jugada = 2;
        }else if(tabla[2] == jugador && tabla[5] == jugador && tabla[8] == 0){
            jugada = 9;
        }else if(tabla[2] == jugador && tabla[5] == 0 && tabla[8] == jugador){
            jugada = 6;
        }else if(tabla[2] == 0 && tabla[5] == jugador && tabla[8] == jugador){
            jugada = 3;
        }else if(tabla[0] == jugador && tabla[4] == jugador && tabla[8] == 0){
            jugada = 9;
        }else if(tabla[0] == jugador && tabla[4] == 0 && tabla[8] == jugador){
            jugada = 5;
        }else if(tabla[0] == 0 && tabla[4] == jugador && tabla[8] == jugador){
            jugada = 1;
        }else if(tabla[2] == jugador && tabla[4] == jugador && tabla[6] == 0){
            jugada = 7;
        }else if(tabla[2] == jugador && tabla[4] == 0 && tabla[6] == jugador){
            jugada = 5;
        }else if(tabla[2] == 0 && tabla[4] == jugador && tabla[6] == jugador){
            jugada = 3;
        }else{
            jugada = 0;
        }
        
        return jugada;
    }

    public void play() {

        //  LOGICA JUEGO PARA JUGAR CONTRA MAQUINA.

        int num = 0;
        
        do{
            //Preguntamos al jugador 1 donde quiere poner ficha.
            do{
                System.out.println("Jugador introduce en que posicion quieres poner la ficha (1-9)");
                draw();
                System.out.println();
                drawTabla();
                try {
                    num = lector.nextInt();
                    if (num > 0 && num < 10) {
                        if (getMap(num - 1) == 0) {
                            setMap(num - 1, 1);
                            break;
                        }else{
                            System.out.println("Posicion ya ocupada!!!");
                        }
                    }
                } catch (Exception e) {
                    lector.next();
                    System.out.println("ERROR");
                }
            }while(true);

            //Parte IA
            if (winner() == 0) {
                System.out.println("TURNO DE LA MAQUINA.");
                setMap(logicaInteligenciaArtificial(), 2);
            }
            
            
            
        }while(winner() == 0);
        mostrarGanador(winner());
        draw();



        //      LOGICA JUEGO PARA 2 JUGADORES

        /*
        int num = 0;
        int turno = 1;

        //Mostramos el tablero inicial.
        draw();
        do{
            //Preguntamos al jugador 1 donde quiere poner ficha.
            do{
                System.out.println("Jugador " + turno + " introduce en que posicion quieres poner la ficha (1-9)");
                drawTabla();
                try {
                    num = lector.nextInt();
                    if (num > 0 && num < 10) {
                        if (getMap(num - 1) == 0) {
                            setMap(num - 1, turno);
                            break;
                        }else{
                            System.out.println("Posicion ya ocupada!!!");
                        }
                    }
                } catch (Exception e) {
                    lector.next();
                    System.out.println("ERROR");
                }
            }while(true);
            //Cambiamos el turno.
            turno = cambiarTurno(turno);
            //Mostramos el tablero.
            draw();
            //Comprobar si no quedan posiciones vacias.
            
            //Comprobar ganador.
            
        }while(winner() == 0);
        mostrarGanador(winner());*/
    }
}