class MotoUtil{


    public static void informe(Moto [] motos){
        Moto motoPotente = masPotente(motos);
        Moto motoCara = masCara(motos);

        double preuConjunt = 0;

        for (int i = 0; i < motos.length; i++) {
            preuConjunt += motos[i].getPrecio();
        }

        System.out.println("La moto mas cara de todas es la " + motoCara.getMarca() + " " + motoCara.getModelo() + " (" + motoPotente.getPrecio() + ")");
        System.out.println("La moto pas potente de todas es la " + motoPotente.getMarca() + " " + motoPotente.getModelo() + " (" + motoPotente.getPotencia() + ")");
        System.out.println("El precio conjunto de las motos es de " + preuConjunt);

    }

    public static void compara(Moto moto1, Moto moto2){

        /**
        Moto [] motComp = {moto1, moto2};

        Moto motPotente = masPotente(motComp);
        
        Moto motCara = masCara(motComp);

        System.out.println("La " + motPotente.getMarca() + " " + motPotente.getModelo() + " tiene " );
        System.out.println("La moto mas cara es " + motCara.getMarca() + " " + motCara.getModelo());
         */

        //Condicional para comparar potencia.
        Moto [] motComp = {moto1, moto2};

        if (masPotente(motComp).getMarca() == moto1.getMarca()) {
            int pot = moto1.getPotencia() - moto2.getPotencia();
            System.out.println("La " + moto1.getMarca() + " " + moto1.getModelo() + " tiene " + pot + "cv mas de potencia que la " + moto2.getMarca() + " " + moto2.getModelo());
        } else {
            int pot = moto2.getPotencia() - moto1.getPotencia();
            System.out.println("La " + moto2.getMarca() + " " + moto2.getModelo() + " tiene " + pot + "cv mas de potencia que la " + moto1.getMarca() + " " + moto1.getModelo());
        }

        //Condicional para comparar precio.
        if (masCara(motComp).getMarca() == moto1.getMarca()) {
            double pot = moto1.getPrecio() - moto2.getPrecio();
            System.out.println("La " + moto1.getMarca() + " " + moto1.getModelo() + " es " + pot + "eur mas cara que la " + moto2.getMarca() + " " + moto2.getModelo());
        } else {
            double pot = moto2.getPrecio() - moto1.getPrecio();
            System.out.println("La " + moto2.getMarca() + " " + moto2.getModelo() + " es " + pot + "eur mas cara que la " + moto1.getMarca() + " " + moto1.getModelo());
        } 
    }

    public static Moto masPotente(Moto [] motos){
        Moto motoPotente = motos[0];
        for (int i = 0; i < motos.length; i++) {
            if (motos[i].getPotencia() > motoPotente.getPotencia()) {
                motoPotente = motos[i];
            }
        }
        return motoPotente;
    }

    public static Moto masCara(Moto [] motos){
        Moto motoCara = motos[0];
        for (int i = 0; i < motos.length; i++) {
            if (motos[i].getPrecio() > motoCara.getPrecio()) {
                motoCara = motos[i];
            }
        }
        return motoCara;
    }
}