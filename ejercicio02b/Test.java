class Test {
    public static void main(String[] args) {
        Moto moto1 = new Moto("Honda", "GoldWing", 120, 30000);
        Moto moto2 = new Moto("Ducati", "Multistrada", 160, 17000);
        Moto moto3 = new Moto("Yamaha", "Tmax", 55, 12000);
        Moto moto4 = new Moto("Suzuki", "VStrom 650", 70, 8000);
        Moto[] motos = { moto1, moto2, moto3, moto4 };
        
        MotoUtil mUtil = new MotoUtil();

        Moto motCara, motPot;

        System.out.println();
        mUtil.informe(motos);
        System.out.println();
        mUtil.compara(moto1, moto2);
        System.out.println();
    }
}