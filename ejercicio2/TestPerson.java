package ejercicio2;

class TestPerson{
    public static void main(String[] args) {
        PersonaController control = new PersonaController();

        Persona per1 = new Persona(1, "David", "david@david.com", 24);
        Persona per2 = new Persona(1, "Joshua", "joshua@joshua.com", 25);

        control.mostrarPersona(per1);
        control.mostrarPersona(per2);

        control.compararPersonas(per1, per2);
    }
}