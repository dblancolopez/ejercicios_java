package ejercicio2;

class PersonaController{
    public void mostrarPersona(Persona persona) {
        System.out.println(persona.getNombre() + " tiene " + persona.getEdad() + " años y su email es " + persona.getEmail() );
    }

    public void compararPersonas(Persona persona1, Persona persona2){
        if(persona1.getEdad() > persona2.getEdad()){
            System.out.println(persona1.getNombre() + "(" + persona1.getEdad() + ") es mayor que " + persona2.getNombre() + " (" + persona2.getEdad() + ")" );
        }else if(persona1.getEdad() < persona2.getEdad()){
            System.out.println(persona2.getNombre() + "(" + persona2.getEdad() + ") es mayor que " + persona1.getNombre() + " (" + persona1.getEdad() + ")" );
        }else{
            System.out.println(persona1.getNombre() + "(" + persona1.getEdad() + ") tiene la misma edad que " + persona2.getNombre() + " (" + persona2.getEdad() + ")" );
        }
    }
}