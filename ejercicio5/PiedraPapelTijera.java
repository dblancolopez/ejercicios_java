import java.util.Scanner;
import java.util.Random;

class PiedraPapelTijera {
    public static void main(String[] args) {
    Scanner lector = new Scanner(System.in);
    int rondasGanadas = 0;
    int rondasGanadasMaquina = 0;
    String nombreJugador;
    Jugador jug1;
    Random random = new Random();
    int jugada = 0;
    int jugadaMaquina = 0;
    String jugadaMaquinaEscrita;

    System.out.println("Introduce tu nombre: ");
    nombreJugador = lector.next();
    jug1 = new Jugador(nombreJugador);

    for (int i = 0; i < 5; i++) {
        boolean correcto = false;
        do{
            System.out.println("Introduce que jugada quieres hacer " + jug1.nombre  + "\n" +
        "1.Piedra\n" +
        "2.Papel\n" +
        "3.Tijeras\n");
        try {
            jugada = lector.nextInt();
            if(jugada == 1 || jugada == 2 || jugada == 3){
                correcto = true;
            }
        } catch (Exception e) {
            jugada = 0;
            System.out.println("Dato introducido erroneo.");
        }
        }while(!correcto);
        
        
        jugadaMaquina = random.nextInt(3)+1;

        if(jugadaMaquina == 1){
            jugadaMaquinaEscrita = "Piedra";
        }else if(jugadaMaquina == 2){
            jugadaMaquinaEscrita = "Papel";
        }else{
            jugadaMaquinaEscrita = "Tijeras";
        }

        System.out.println("La maquina ha sacado: " + jugadaMaquinaEscrita + "\n");

        if (jugada == 1 && jugadaMaquina == 1 || 
        jugada == 2 && jugadaMaquina == 2 || 
        jugada == 3 && jugadaMaquina == 3) {
            jug1.empates++;
            System.out.println("Habeis empatado esta ronda. \n");
            System.out.println("MARCADOR\nJugador: " + rondasGanadas + "\nMaquina: " + rondasGanadasMaquina);
        } else if (jugada == 1 && jugadaMaquina == 2 || 
        jugada == 2 && jugadaMaquina == 3 || 
        jugada == 3 && jugadaMaquina == 1){
            rondasGanadasMaquina++;
            System.out.println("Has perdido esta ronda.\n");
            System.out.println("MARCADOR\nJugador: " + rondasGanadas + "\nMaquina: " + rondasGanadasMaquina);
        }else if(jugada == 2 && jugadaMaquina == 1 || 
        jugada == 3 && jugadaMaquina == 2 || 
        jugada == 1 && jugadaMaquina == 3){
            jug1.ganadas++;
            System.out.println("has ganado esta ronda.\n");
            System.out.println("MARCADOR\nJugador: " + rondasGanadas + "\nMaquina: " + rondasGanadasMaquina);
        }

        
        jug1.partidas++;

    }
    if (rondasGanadas > rondasGanadasMaquina) {
        System.out.println("Has ganado la partida " + jug1.ganadas + " a " + rondasGanadasMaquina);
    }else if (rondasGanadasMaquina > rondasGanadas){
        System.out.println("Has perdido la partida " + jug1.ganadas+ " a " + rondasGanadasMaquina);
    }else{
        System.out.println("Habeis empatado " + jug1.ganadas + " a " + rondasGanadasMaquina);
    }
    lector.close();
    }
}