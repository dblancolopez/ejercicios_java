class Moto{
    String model;
    int preu;
    int kilometres;
    int cilindrada;
    String lloc;
    int any;

    public Moto(String model, int preu, int kilometres, int cilindrada, String lloc, int any) {
        this.model = model;
        this.preu = preu;
        this.kilometres = kilometres;
        this.cilindrada = cilindrada;
        this.lloc = lloc;
        this.any = any;
    }

    @Override
    public String toString() {
        return "Moto [any=" + any + ", cilindrada=" + cilindrada + ", kilometres=" + kilometres + ", lloc=" + lloc
                + ", model=" + model + ", preu=" + preu + "]";
    }
    
}