import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import com.google.gson.Gson;

class testMotos {
    public static void main(String[] args) {
        File jsonFile = new File("motos.json");
        StringWriter sw = new StringWriter();
        try (FileReader fr = new FileReader(jsonFile); BufferedReader br = new BufferedReader(fr);) {
            int size = 100;
            char[] buffer = new char[size];
            int len;
            while ((len = br.read(buffer, 0, size)) > -1) {
                sw.write(buffer, 0, len);
            }
        } catch (IOException e) {
            //
            e.printStackTrace();
        }

        // System.out.println(sw.toString());
        Gson gson = new Gson();
        Moto[] motos = gson.fromJson(sw.toString(), Moto[].class);

        for (Moto motox : motos) {
            System.out.println(motox.toString());
        }
    }
}