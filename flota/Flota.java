import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

class Flota {
    private static ArrayList<Coche> cars = new ArrayList<Coche>();
    private static String[] marcas = { "Audi", "Aston Martin", "Mercedes", "BMW" };
    private static String[] colores = { "Rojo", "Azul", "Verde", "Naranja" };

    public static void generarCoches(int num) {
        /*
         * colores = new String[4]; marcas = new String[4]; colores[0] = "Rojo";
         * colores[1] = "Azul"; colores[2] = "Verde"; colores[3] = "Naranja"; marcas[0]
         * = "Audi"; marcas[1] = "Aston Martin"; marcas[2] = "Mercedes"; marcas[3] =
         * "BMW";
         */

        Random random = new Random();

        for (int i = 0; i < num; i++) {
            int color = random.nextInt(4);
            int marca = random.nextInt(4);
            int kilometros = random.nextInt(150000) + 0;

            Coche coche = new Coche(marcas[marca], colores[color], kilometros);
            cars.add(coche);
        }
    }

    public static void resumen() {
        int numAudi = 0;
        int numAstonMartin = 0;
        int numMercedes = 0;
        int numBmw = 0;
        String msj = "";

        int numRojo = 0;
        int numAzul = 0;
        int numVerde = 0;
        int numNaranja = 0;
        String msj2 = "";

        int carsCienMil = 0;



        Map<String, AtomicInteger> numMap = new TreeMap<String, AtomicInteger>();

        for (Coche car : cars) {
            AtomicInteger count = numMap.get(car.getMarca());
            if (count == null) {
                count = new AtomicInteger(0);
                numMap.put(car.getMarca(), count);
            }
            count.incrementAndGet();
        }

        for (Map.Entry<String, AtomicInteger> entry : numMap.entrySet()) {
            System.out.print("Ocurrencias de " + entry.getKey() + ": ");
            System.out.println(entry.getValue());
        }





        for (Coche var : cars) {
            if (var.getMarca().equals("Audi")) {
                numAudi++;
            } else if (var.getMarca().equals("Aston Martin")) {
                numAstonMartin++;
            } else if (var.getMarca().equals("Mercedes")) {
                numMercedes++;
            } else if (var.getMarca().equals("BMW")) {
                numBmw++;
            }
        }

        if (numAudi > numAstonMartin && numAudi > numMercedes && numAudi > numBmw) {
            msj = "La marca mas comun es Audi con " + numAudi + " coches";
        } else if (numAstonMartin > numAudi && numAstonMartin > numMercedes && numAstonMartin > numBmw) {
            msj = "La marca mas comun es Aston Martin con " + numAstonMartin + " coches";
        } else if (numMercedes > numAudi && numMercedes > numAstonMartin && numMercedes > numBmw) {
            msj = "La marca mas comun es Mercedes con " + numMercedes + " coches";
        } else {
            msj = "La marca mas comun es BMW con " + numBmw + " coches";
        }

        for (Coche var : cars) {
            if (var.getColor().equals("Rojo")) {
                numRojo++;
            } else if (var.getColor().equals("Azul")) {
                numAzul++;
            } else if (var.getColor().equals("Verde")) {
                numVerde++;
            } else if (var.getColor().equals("Naranja")) {
                numNaranja++;
            }
        }
        if (numRojo > numAzul && numRojo > numVerde && numRojo > numNaranja) {
            msj2 = "El color mas frecuente es el Rojo con " + numRojo + " coches";
        } else if (numAzul > numRojo && numAzul > numVerde && numAzul > numNaranja) {
            msj2 = "El color mas frecuente es el Azul con " + numAzul + " coches";
        } else if (numVerde > numRojo && numVerde > numAzul && numVerde > numNaranja) {
            msj2 = "El color mas frecuente es el Verde con " + numVerde + " coches";
        } else if (numNaranja > numRojo && numNaranja > numAzul && numNaranja > numVerde) {
            msj2 = "El color mas frecuente es el Naranja con " + numNaranja + " coches";
        }

        // Bucle para comprobar los coches q sobrepasan los 100.000 Km.
        for (Coche var : cars) {
            if (var.getKm() > 100000) {
                carsCienMil++;
            }
        }

        System.out.println(cars.size() + " coches en la lista.");
        System.out.println(msj);
        System.out.println(msj2);
        System.out.println(carsCienMil + " coches tienen mas de 100.000 Km.");
    }

    public static void consultarMarca(String marca) {
        for (Coche var : cars) {
            if (var.getMarca().equals(marca)) {
                System.out.println(var.toString());
            }
        }
    }

    public static void consultarColor(String color) {
        for (Coche var : cars) {
            if (var.getColor().equals(color)) {
                System.out.println(var.toString());
            }
        }
    }

    public static void consultarKm(int kms) {
        for (Coche var : cars) {
            if (var.getKm() >= kms) {
                System.out.println(var.toString());
            }
        }
    }

    public static void consultarMultiple(String marca, String color, int kms) {
        for (Coche var : cars) {
            if (var.getMarca().equals(marca) && var.getColor().equals(color) && var.getKm() >= kms) {
                System.out.println(var.toString());
            }
        }
    }
}