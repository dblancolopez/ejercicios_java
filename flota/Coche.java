class Coche{
    private String marca;
    private String color;
    private int km;

    public Coche(String marca, String color, int km) {
        this.marca = marca;
        this.color = color;
        this.km = km;
    }

    @Override
    public String toString() {
        return "Coche [color=" + color + ", km=" + km + ", marca=" + marca + "]";
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    
}