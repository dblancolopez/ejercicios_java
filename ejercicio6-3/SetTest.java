import java.util.HashSet;
import java.util.Set;

class SetTest {
    public static void main(String[] args) {
        Set<Company> empresas = new HashSet<Company>();

        Company e1 = new Company("Oracle", 1977);
        Company e2 = new Company("IBM", 1911);
        Company e3 = new Company("Oracle", 1977);
        Company e4 = new Company("IBM", 1977);
        Company e5 = new Company("IBM", 1967);

        empresas.add(e1);
        empresas.add(e2);
        empresas.add(e3);
        empresas.add(e4);
        empresas.add(e5);
        
        int tamaño = empresas.size();
        System.out.println(tamaño);
    }
}