import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

class ListTest {
    public static void main(String[] args) {
        List<Integer> lista2 = Arrays.asList(1,2,3,4,5);
        AtomicInteger i = new AtomicInteger(0);


        System.out.println(lista2.get(1));

        for (int j = 0; j < lista2.size(); j++) {
            if (lista2.get(j) % 2 != 0) {
               i.addAndGet(lista2.get(j));             
            }
        }
        System.out.println(i);
    }
}