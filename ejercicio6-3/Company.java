class Company{
    String name;
    int year;

    public Company(String name, int year) {
        this.name = name;
        this.year = year;
    }


    @Override
    public boolean equals(Object o) {
        Company otra = (Company) o;

        if(this.name.equals(otra.name) && this.year == otra.year){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
    
}