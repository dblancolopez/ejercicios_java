import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

class Filtra {
    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Archivo no especificado.");
            return;
        }

        String archivo = "motos.csv";
        File file = new File(archivo);
        String archivo2 = args[0] + ".csv";
        File dhFile = new File(archivo2);

        try (FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
                FileWriter fw = new FileWriter(dhFile);
                BufferedWriter bw = new BufferedWriter(fw)) {

            String line;
            do {
                line = br.readLine();
                if (line != null) {
                    if (line.toLowerCase().contains(args[0])) {
                        bw.write(line);
                        bw.newLine();
                    }
                }
            } while (line != null);

            bw.flush();
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("FALLO");
        }

    }
}