class Ejercicio1{
    public static void main(String[] args) {
        Conversor conversor1 = new Conversor();

        String medida1 = "km/h", medida2 = "mi/h";
        double valor = 10.0;
        
        System.out.println(conversor1.operacion(medida1, medida2, valor) + " " + medida2 + " desde " + valor+ " " + medida1);
    }
}