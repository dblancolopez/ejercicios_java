package com.codesplai.pipatiweb;

public class HtmlFactory {
    public String titulo(String contenido) {
        String plantilla = "<h1>%s<h1>";
        return String.format(plantilla, contenido);
    }
}