<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.pipatiweb.HtmlFactory" %>
<%
HtmlFactory factory = new HtmlFactory();
%>
<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
    <%= factory.titulo("Piedra Papel Tijera JSP") %>
</body>

</html>