<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.codesplai.pipatiweb.Pipati" %>
<%@page import="com.codesplai.pipatiweb.HtmlFactory" %>
<%
HtmlFactory factory = new HtmlFactory();
int jugada_usuario = Integer.parseInt(request.getParameter("jugada")); //1
String respuesta = Pipati.partida(jugada_usuario); //2
%>
<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="utf-8">
    <title>Java demo</title>
    <link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>

<body>
    <%= factory.titulo(respuesta) %>
    <a href="index.jsp">Volver a jugar</a>
</body>

</html>